﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using RecipesApp.Recipes.Models;
using RecipesApp.Recipes.Services;
using SUT = RecipesApp.Controllers;

using Xunit;

namespace RecipesApp.Tests.Controllers.RecipesController
{
    public class UpdateRecipe
    {
        private readonly SUT.RecipesController _controller;
        private readonly IRecipeService _service = Substitute.For<IRecipeService>();

        public UpdateRecipe()
        {
            this._controller = new SUT.RecipesController(this._service);
        }

        [Fact]
        public async Task ReturnsUpdatedRecipe_WhenOneExistsWithGivenId()
        {
            var updatedRecipe = TestData.Recipes.List[1];
            var id = updatedRecipe.Id;
            this._service.UpdateRecipeAsync(recipeId: Arg.Any<Guid>(), recipe: Arg.Any<Recipe>()).Returns(updatedRecipe);
            var result = await this._controller.UpdateRecipe(id, updatedRecipe);
            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);
            var objectResult = result as OkObjectResult;
            Assert.IsType<Recipe>(objectResult?.Value);
            var recipe = objectResult.Value as Recipe;
            Assert.Equal(updatedRecipe, recipe);
        }
    }
}