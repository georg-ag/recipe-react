﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using RecipesApp.Recipes.Models;
using RecipesApp.Recipes.Services;
using SUT = RecipesApp.Controllers;
using Xunit;

namespace RecipesApp.Tests.Controllers.RecipesController
{
    public class GetRecipes
    {
            private readonly SUT.RecipesController _controller;
            private readonly IRecipeService _service = Substitute.For<IRecipeService>();

            public GetRecipes()
            {
                this._controller = new SUT.RecipesController(this._service);
            }

            [Fact]
            public async Task ReturnsRecipes()
            {
                this._service.GetUserRecipesAsync().Returns(TestData.Recipes.List);
                var result = await this._controller.GetRecipes();
                Assert.NotNull(result);
                Assert.IsType<OkObjectResult>(result);
                var objectResult = result as OkObjectResult;
                Assert.IsType<List<Recipe>>(objectResult?.Value);
                var recipes = objectResult.Value as List<Recipe>;
                Assert.Equal(TestData.Recipes.List, recipes);
            }
    }
}
