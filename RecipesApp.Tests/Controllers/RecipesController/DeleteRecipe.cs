﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using RecipesApp.Recipes.Models;
using RecipesApp.Recipes.Services;
using SUT = RecipesApp.Controllers;

using Xunit;

namespace RecipesApp.Tests.Controllers.RecipesController
{
    public class DeleteRecipe
    {
        private readonly SUT.RecipesController _controller;
        private readonly IRecipeService _service = Substitute.For<IRecipeService>();

        public DeleteRecipe()
        {
            this._controller = new SUT.RecipesController(this._service);
        }

        [Fact]
        public async Task ReturnsNoContent_WhenOneExistsWithGivenId()
        {
            var result = await this._controller.DeleteRecipe(Guid.NewGuid());
            Assert.NotNull(result);
            Assert.IsType<NoContentResult>(result);
        }
    }
}