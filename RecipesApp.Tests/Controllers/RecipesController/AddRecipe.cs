﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using RecipesApp.Recipes.Models;
using RecipesApp.Recipes.Services;
using SUT = RecipesApp.Controllers;

using Xunit;

namespace RecipesApp.Tests.Controllers.RecipesController
{
    public class AddRecipe
    {
        private readonly SUT.RecipesController _controller;
        private readonly IRecipeService _service = Substitute.For<IRecipeService>();

        public AddRecipe()
        {
            this._controller = new SUT.RecipesController(this._service);
        }

        [Fact]
        public async Task ReturnsAddedRecipe_WhenRecipeIsValid()
        {
            var addedRecipe = TestData.Recipes.List[0];
            var request = new CreateRecipe
            {
                Title = addedRecipe.Title,
                Description = addedRecipe.Description,
                ImageUrl = addedRecipe.ImageUrl,
                Ingredients = addedRecipe.Ingredients,
                Instructions = addedRecipe.Instructions,
                IsShared = addedRecipe.IsShared
            };
            this._service.AddRecipeAsync(request: Arg.Any<CreateRecipe>()).Returns(addedRecipe);
            var result = await this._controller.AddRecipe(request);
            Assert.NotNull(result);
            Assert.IsType<CreatedAtActionResult>(result);
            var createdResult = result as CreatedAtActionResult;
            Assert.Equal(addedRecipe.Id, createdResult?.RouteValues.Values.First());
            Assert.IsType<Recipe>(createdResult?.Value);
            var recipe = createdResult.Value as Recipe;
            Assert.Equal(addedRecipe, recipe);
        }

        [Fact]
        public async Task ReturnsBadRequest_WhenRecipeIsNull()
        {
            var result = await this._controller.AddRecipe(null);
            Assert.NotNull(result);
            Assert.IsType<BadRequestObjectResult>(result);
            var badRequestResult = result as BadRequestObjectResult;
            Assert.Equal("invalid recipe received.", badRequestResult?.Value);
        }
    }
}