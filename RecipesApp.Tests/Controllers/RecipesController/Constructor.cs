﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using RecipesApp.Recipes.Services;
using Xunit;

namespace RecipesApp.Tests.Controllers.RecipesController
{
    public class Constructor
    {
        [Fact]
        public void Creates()
        {
            var mockService = Substitute.For<RecipeService>();
            var controller = new RecipesApp.Controllers.RecipesController(mockService);
            Assert.NotNull(controller);
        }
    }
}
