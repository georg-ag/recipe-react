﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using RecipesApp.Recipes.Models;
using RecipesApp.Recipes.Services;
using SUT = RecipesApp.Controllers;

using Xunit;

namespace RecipesApp.Tests.Controllers.RecipesController
{
    public class GetRecipeById
    {
        private readonly SUT.RecipesController _controller;
        private readonly IRecipeService _service = Substitute.For<IRecipeService>();

        public GetRecipeById()
        {
            this._controller = new SUT.RecipesController(this._service);
        }

        [Fact]
        public async Task ReturnsRecipe_WhenOneExistsWithGivenId()
        {
            this._service.GetRecipeByIdAsync(recipeId: Arg.Any<Guid>()).Returns(TestData.Recipes.List[0]);
            var result = await this._controller.GetRecipeById(Guid.NewGuid());
            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);
            var objectResult = result as OkObjectResult;
            Assert.IsType<Recipe>(objectResult?.Value);
            var recipe = objectResult.Value as Recipe;
            Assert.Equal(TestData.Recipes.List[0], recipe);
        }
    }
}
