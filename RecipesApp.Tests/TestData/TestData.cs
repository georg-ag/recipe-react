using System;
using System.Collections.Generic;
using RecipesApp.Recipes.Models;

namespace RecipesApp.Tests.TestData
{
    public static class Recipes
    {
        public static List<Recipe> List { get; } = new()
        {
            new Recipe()
            {
                Id = Guid.NewGuid(),
                Title = "Burger",
                Description = "Harp Style Burger",
                ImageUrl = "https://live.staticflickr.com/5171/5441630250_419a82ee88_b.jpg",
                CreatedTimeStamp = new DateTimeOffset(2021, 3, 12, 13, 3, 40, new TimeSpan(0)),
                ChangedTimeStamp = new DateTimeOffset(2021, 3, 18, 8, 33, 9, new TimeSpan(2, 0, 0)),
                Instructions = @"Das Faschierte vom Rind mit Salz, Pfeffer, Kräutersalz, Knoblauchgranulat, Zwiebelgranulat und Tabasco in eine Schüssel geben
und gut miteinander verkneten - am besten mit feuchten Händen oder mit einem Mixer mit Knethakenaufsatz.
Danach den Fleischteig für gut 15 Minuten ziehen lassen. Anschließend mit den Händen Patties formen –
dabei darauf achten, dass man die Laibchen nicht zu stark mit den Händen presst, damit sie beim Braten nicht zu fest werden.
Zum Schluss die Fleichlaibchen, in einer Pfanne mit ganz wenig Öl, bei mehrmaligem Wenden mit einem Pfannenwender, ausbraten.",
                IsShared = false,
                UserId = new Guid("e3d72486-cf4f-44c2-aa80-6dd0a07f22c3"),
                Ingredients = new List<Ingredient>() {
                    new Ingredient(){
                        Id = 0,
                        Name = "Buns",
                        Details = "with sesame",
                        Unit = "p",
                        Quantity = 4,
                    },
                    new Ingredient(){
                        Id = 1,
                        Name = "Meat",
                        Details = "ground beef",
                        Unit = "g",
                        Quantity = 200,
                    },
                    new Ingredient(){
                        Id = 2,
                        Name = "Tomatoes",
                        Details = "",
                        Unit = "g",
                        Quantity = 100,
                    },
                    new Ingredient(){
                        Id = 3,
                        Name = "Cheddar",
                        Details = "sliced",
                        Unit = "",
                        Quantity = 4,
                    },
                }
            },
            new Recipe()
            {
                Id = Guid.NewGuid(),
                Title = "Pizza",
                Description = "Margheritha",
                ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/a/a3/Eq_it-na_pizza-margherita_sep2005_sml.jpg",
                CreatedTimeStamp = new DateTimeOffset(2021, 8, 30, 4, 20, 0, new TimeSpan(0)),
                ChangedTimeStamp = new DateTimeOffset(2021, 9, 1, 16, 50, 18, new TimeSpan(4, 0, 0)),
                IsShared = true,
                UserId = new Guid("8341a755-624d-4662-bf2e-cea74c793f59"),
                Instructions = @"Prepare Pizza Dough: In a medium bowl, whisk together the all-purpose flour, sugar, yeast and salt. Add the warm water and olive oil, and stir the mixture with a wooden spoon until the dough just begins to come together. It will seem shaggy and dry, but don’t worry.
Scrape the dough onto a well-floured counter top and knead the dough for three minutes. It should quickly come together and begin to get sticky. Dust the dough with flour as needed (sometimes I will have to do this 2 to 3 times, depending on humidity levels) – it should be slightly tacky, but should not be sticking to your counter top.  After about 3 minutes, the dough should be smooth, slightly elastic, and tacky. Lightly grease a large mixing bowl with olive oil, and place the dough into the bowl.
Cover the bowl with a kitchen towel (or plastic wrap) and allow the dough to rise in a warm, dry area of your kitchen for 2 hours or until the dough has doubled in size. Proofing Tip: If your kitchen is very cold, heat a large heatproof measuring cup of water in the microwave for 2 to 3 minutes. This creates a nice warm environment. Remove the cup and place the bowl with the dough in the microwave until it has risen. [If you are preparing the dough in advance, see the note section for freezing instructions.]
Preheat Oven and Pizza Steel or Stone: Place the pizza steel (or stone) on the second to top rack of your oven (roughly 8 inches from the broiler element), and preheat the oven and steel (or stone) to 550°F (285°C) for a minumum of 1 hour. If your oven does not go up to 550°F (285°C) or you are using a delicate pizza stone, I recommend heating it to a maximum of 500°F (260°C)
As the oven is preheating, assemble the ingredients. In a small bowl, stir together the pureed tomatoes, minced garlic, extra virgin olive oil, pepper, and salt. Set aside another small bowl with the cubed mozzarella cheese (pat the cheese with a paper towel to remove any excess moisture). Set aside the basil leaves and grated parmigiano-reggiano cheese for easy grabbing.
Separate the dough into two equal-sized portions. It will deflate slightly, but that is OK. Place the dough on a large plate or floured counter top, cover gently with plastic wrap, and allow the dough to rest for 5 to 10 minutes.
Assemble the Pizza: Sprinkle the pizza peel (if you do not own a pizza peel, you can try using the back of a half sheet pan - but it is tricky!) with a tablespoon of semolina and dusting of all-purpose flour. Gently use both hands to stretch one ball of pizza dough into roughly a 10-inch circle (don’t worry if its not perfectly uniform). If the dough springs back or is too elastic, allow it to rest for an additional five minutes. The edges of the dough can be slightly thicker, but make sure the center of the dough is thin (you should be able to see some light through it if you held it up). Gently transfer the dough onto the semolina and flour dusted pizza peel or baking sheet.
Drizzle or brush the dough lightly (using your fingertips) with olive oil (roughly a teaspoon. Using a large spoon, add roughly ½ cup of the tomato sauce onto the pizza dough, leaving a ½-inch or ¾-inch border on all sides. Use the back of the spoon to spread it evenly and thinly. Sprinkle a tablespoon of parmigiano-reggiano cheese onto the pizza sauce. Add half of the cubed mozzarella, distributing it evenly over the entire pizza. Using your hands, tear a few large basil leaves, and sprinkle the basil over the pizza. At this point, I’ll occasionally stretch the sides of the dough out a bit to make it even thinner. Gently slide the pizza from the peel onto the heated baking stone. Bake for 7 to 8 minutes, or until the crust is golden and the cheese is bubbling and caramelized and the edges of the pizza are golden brown. Note: If you're looking for more color, finish the pizza under the low or medium broil setting, but watch it carefully!
Remove the pizza carefully from the oven with the pizza peel, transfer to a wooden cutting board or foil, drizzle the top with olive oil, some grated parmigiano-reggiano cheese, and chiffonade of fresh basil. Slice and serve immediately and/or prepare the second pizza.
Serving Tip: If you’re serving two pizzas at once, I recommend placing the cooked pizza on a separate baking sheet while you prepare the other pizza. In the last few minutes of cooking, place the prepared pizza into the oven (on a rack below the pizza stone) so that it is extra hot for serving. Otherwise, I recommend serving one pizza fresh out of the oven, keeping the oven hot, and preparing the second pizza after people have gone through the first one! The pizza will taste great either way, but it is at its prime within minutes out of the oven!.",
                Ingredients = new List<Ingredient>() {
                    new Ingredient(){
                        Id = 0,
                        Name = "Flour",
                        Details = "Typ 00",
                        Unit = "g",
                        Quantity = 250,
                    },
                    new Ingredient(){
                        Id = 1,
                        Name = "Tomatoes",
                        Details = "canned",
                        Unit = "g",
                        Quantity = 150,
                    },
                    new Ingredient(){
                        Id = 2,
                        Name = "Mozzarella",
                        Details = "",
                        Unit = "g",
                        Quantity = 100,
                    },
                    new Ingredient(){
                        Id = 3,
                        Name = "Yeast",
                        Details = "dry",
                        Unit = "teaspoon",
                        Quantity = 1.0/2,
                    },
                }
            },
            new Recipe()
            {
                Id = Guid.NewGuid(),
                Title = "Dummy",
                Description = "Dummy",
                ImageUrl = "https://live.staticflickr.com/4078/4929813617_6d080baa93.jpg",
                CreatedTimeStamp = new DateTimeOffset(2021, 4, 30, 4, 20, 0, new TimeSpan(0)),
                ChangedTimeStamp = new DateTimeOffset(2021, 5, 1, 16, 50, 18, new TimeSpan(4, 0, 0)),
                IsShared = true,
                UserId = new Guid("8341a755-624d-4662-bf2e-cea74c793f59"),
                Instructions = @"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
                Ingredients = new List<Ingredient>() {
                    new Ingredient(){
                        Id = 0,
                        Name = "Flour",
                        Details = "Typ 00",
                        Unit = "g",
                        Quantity = 250,
                    },
                    new Ingredient(){
                        Id = 1,
                        Name = "Tomatoes",
                        Details = "canned",
                        Unit = "g",
                        Quantity = 150,
                    },
                    new Ingredient(){
                        Id = 2,
                        Name = "Mozzarella",
                        Details = "",
                        Unit = "g",
                        Quantity = 100,
                    },
                    new Ingredient(){
                        Id = 3,
                        Name = "Yeast",
                        Details = "dry",
                        Unit = "teaspoon",
                        Quantity = 1.0/2,
                    },
                }
            }
        };
    }
}