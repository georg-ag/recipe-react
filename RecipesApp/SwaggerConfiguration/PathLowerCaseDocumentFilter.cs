using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace RecipesApp.SwaggerConfiguration
{
    /// <summary>
    /// A filter to enforce lower case paths in swagger documentation
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PathLowercaseDocumentFilter : IDocumentFilter
    {
        /// <summary>
        /// Converts all paths in the swagger documentation to lower case
        /// </summary>
        /// <param name="swaggerDoc">The auto generated Document</param>
        /// <param name="context">The Context</param>
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            var dictionaryPath = swaggerDoc.Paths.ToDictionary(x => ToLowercase(x.Key), x => x.Value);
            var newPaths = new OpenApiPaths();
            foreach (var path in dictionaryPath)
            {
                newPaths.Add(path.Key, path.Value);
            }
            swaggerDoc.Paths = newPaths;
        }

        private static string ToLowercase(string key)
        {
            var parts = key.Split('/').Select(part => part.Contains("}") ? part : part.ToLowerInvariant());
            return string.Join('/', parts);
        }
    }
}