import Recipe from '../shared/models/Recipe';

export const mockRecipes: Recipe[] = [
	{
		id: '1',
		title: 'Dummy Recipe',
		description: 'Just for testing',
		imageUrl: 'url.fake',
		ingredients: [
			{ id: 0, name: 'Carrots', details: 'for rabbit', quantity: 10, unit: 'g' },
			{ id: 1, name: 'Onions', details: '', quantity: 0, unit: '' },
		],
		instructions: 'Test the recipe.',
		createdTimeStamp: '2021-10-05',
		changedTimeStamp: '2021-10-07',
		isShared: false,
		userId: 'userGuid',
	},
	{
		id: '2',
		title: 'Also Recipe',
		description: 'Only for testing',
		imageUrl: 'url2.fake',
		ingredients: [
			{ id: 0, name: 'Rice', details: 'Basmati', quantity: 150, unit: 'g' },
			{ id: 1, name: 'Onions', details: '', quantity: 2, unit: '' },
		],
		instructions: 'Cook the rice',
		createdTimeStamp: '2021-11-25',
		changedTimeStamp: '2021-12-23',
		isShared: true,
		userId: 'userGuid2',
	},
	{
		id: '3',
		title: 'Last Recipe',
		description: 'No more after this one',
		imageUrl: 'url.faker',
		ingredients: [{ id: 0, name: 'Pizza', details: 'frozen', quantity: 1, unit: '' }],
		instructions: 'Prepare according to instrutions on packaging',
		createdTimeStamp: '2021-12-06',
		changedTimeStamp: '2021-12-10',
		isShared: false,
		userId: 'userGuid',
	},
];
