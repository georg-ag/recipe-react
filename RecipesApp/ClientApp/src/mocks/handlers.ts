import { rest } from 'msw';
import { mockRecipes } from './recipes';

const baseUrl = 'https://localhost:44302/api';
const singleRecipeUrl = baseUrl + '/recipes/:recipeId';

export const handlers = [
	rest.get(baseUrl + '/recipes', (req, res, ctx) => {
		return res(ctx.status(200), ctx.json(mockRecipes));
	}),

	rest.get(singleRecipeUrl, (req, res, ctx) => {
		const { recipeId } = req.params;
		const recipe = mockRecipes.find((mock) => (mock.id = recipeId));
		return res(ctx.status(200), ctx.json(recipe));
	}),

	rest.post(baseUrl + '/recipes', (req, res, ctx) => {
		return res(ctx.status(201), ctx.json({ ...mockRecipes[1], id: '4' }));
	}),

	rest.put(singleRecipeUrl, (req, res, ctx) => {
		const recipe = req.body;
		return res(ctx.status(200), ctx.json(recipe));
	}),

	rest.delete(singleRecipeUrl, (req, res, ctx) => {
		return res(ctx.status(204));
	}),
];
