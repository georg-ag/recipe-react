import { ThemeProvider } from '@mui/material/styles';
import { ConnectedRouter } from 'connected-react-router';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './App';
import { store, history } from './app/store';
import theme from './theme';

ReactDOM.render(
	<React.StrictMode>
		<Provider store={store}>
			<ConnectedRouter history={history}>
				<ThemeProvider theme={theme}>
					<App />
				</ThemeProvider>
			</ConnectedRouter>
		</Provider>
	</React.StrictMode>,
	document.getElementById('root'),
);
