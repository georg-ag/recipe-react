import { createTheme } from '@mui/material/styles';

const theme = createTheme({
	palette: {
		primary: {
			main: '#c62828',
		},
		secondary: {
			main: '#00695c',
		},
		background: {
			default: '#fff',
			paper: '#fff',
		},
	},
});

export default theme;
