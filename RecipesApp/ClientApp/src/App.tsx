import React from 'react';

import { Switch, Route, Redirect } from 'react-router-dom';
import Header from './components/Layout/Header';
import Home from './components/Home';
import RecipeDetail from './features/recipe/pages/RecipeDetail';
import RecipeEdit from './features/recipe/pages/RecipeEdit';
import RecipeCreation from './features/recipe/pages/RecipeCreation';

function App() {
	return (
		<div className="App">
			<Header />
			<Switch>
				<Route exact path="/">
					<Redirect to="/recipes" />
				</Route>
				<Route path="/recipes" exact component={Home} />
				<Route path="/recipes/new" exact component={RecipeCreation} />
				<Route path="/recipes/:id" exact component={RecipeDetail} />
				<Route path="/recipes/:id/edit" component={RecipeEdit} />
			</Switch>
		</div>
	);
}

export default App;
