export interface Unit {
	name: string;
	baseUnit: BaseUnit;
	conversionFactor: number;
}

export enum BaseUnit {
	liter,
	gramm,
	tablespoon,
	piece,
	pinch,
}
