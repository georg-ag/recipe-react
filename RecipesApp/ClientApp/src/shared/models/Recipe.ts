import Ingredient from './Ingredient';

export default interface Recipe {
	id: string;
	title: string;
	description: string;
	instructions: string;
	imageUrl: string;
	createdTimeStamp: string;
	changedTimeStamp: string;
	ingredients: Ingredient[];
	isShared: boolean;
	userId: string;
}

export const getEmptyRecipe = () => {
	const recipe: Recipe = {
		id: '',
		title: '',
		description: '',
		instructions: '',
		imageUrl: '',
		createdTimeStamp: '',
		changedTimeStamp: '',
		ingredients: [],
		isShared: false,
		userId: '',
	};
	return recipe;
};
