export default interface Ingredient {
	id: number;
	name: string;
	details?: string;
	unit: string;
	quantity: number;
}
