import React, { FC, ReactElement } from 'react';
import { render, RenderOptions } from '@testing-library/react';
import { ThemeProvider } from '@mui/material/styles';
import { ConnectedRouter, connectRouter, routerMiddleware } from 'connected-react-router';
import { createMemoryHistory } from 'history';
import { configureStore } from '@reduxjs/toolkit';
import recipeReducer from '../features/recipe/recipeSlice';
import { Provider } from 'react-redux';
import theme from '../theme';
import { api } from '../features/api/api';

const history = createMemoryHistory();
const store = configureStore({
  reducer: {
    recipe: recipeReducer,
    [api.reducerPath]: api.reducer,
    // necessary because of typing issue
    // @ts-ignore
    router: connectRouter(history),
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(routerMiddleware(history), api.middleware),
});

const AllTheProviders: FC = ({ children }) => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <ThemeProvider theme={theme}>{children}</ThemeProvider>
      </ConnectedRouter>
    </Provider>
  );
};

const customRender = (ui: ReactElement, options?: Omit<RenderOptions, 'wrapper'>) =>
  render(ui, { wrapper: AllTheProviders, ...options });

export * from '@testing-library/react';
export { customRender as render };
