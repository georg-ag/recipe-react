import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import Recipe from '../../shared/models/Recipe';

export const api = createApi({
	reducerPath: 'api',
	baseQuery: fetchBaseQuery({
		baseUrl: 'https://localhost:44302/api/',
		prepareHeaders(headers) {
			headers.set('x-api-key', 'afdgdfgsdfgsdfgsdfgsdfg');
			return headers;
		},
	}),
	tagTypes: ['Recipe'],
	endpoints: (builder) => ({
		getRecipeById: builder.query<Recipe, string>({
			query: (id) => `recipes/${id}`,
			providesTags: (result, error, arg) =>
				result ? [{ type: 'Recipe', id: result.id }, 'Recipe'] : ['Recipe'],
		}),
		getRecipes: builder.query<Recipe[], void>({
			query: () => 'recipes/',
			providesTags: (result, error, arg) =>
				result
					? [
							...result.map(({ id: string }) => ({ type: 'Recipe' as const, id: string })),
							{ type: 'Recipe', id: 'LIST' },
					  ]
					: ['Recipe'],
		}),
		addRecipe: builder.mutation<Recipe, Partial<Recipe>>({
			query: (recipe) => ({
				url: 'recipes/',
				method: 'POST',
				body: recipe,
			}),
			async onQueryStarted(recipe, { dispatch, queryFulfilled }) {
				try {
					const { data: createdRecipe } = await queryFulfilled;
					dispatch(
						api.util.updateQueryData('getRecipeById', createdRecipe.id, (draft) => {
							Object.assign(draft, createdRecipe);
						}),
					);
				} catch {}
			},
			invalidatesTags: [{ type: 'Recipe', id: 'LIST' }],
		}),
		updateRecipe: builder.mutation<Recipe, Recipe>({
			query: (recipe) => ({
				url: `recipes/${recipe.id}`,
				method: 'PUT',
				body: recipe,
			}),
			transformResponse: (response: { data: Recipe }) => response.data,
			async onQueryStarted(recipe, { dispatch, queryFulfilled }) {
				dispatch(
					api.util.updateQueryData('getRecipeById', recipe.id, (draft) => {
						Object.assign(draft, recipe);
					}),
				);
				try {
					await queryFulfilled;
				} catch {
					dispatch(
						api.util.invalidateTags([
							{ type: 'Recipe', id: recipe.id },
							{ type: 'Recipe', id: 'LIST' },
						]),
					);
				}
			},
			invalidatesTags: (result, error, arg) => [{ type: 'Recipe', id: 'LIST' }],
		}),
		deleteRecipe: builder.mutation<void, string>({
			query: (recipeId) => ({
				url: `recipes/${recipeId}`,
				method: 'DELETE',
			}),
			invalidatesTags: (result, error, arg) => [
				{ type: 'Recipe', id: arg },
				{ type: 'Recipe', id: 'LIST' },
			],
		}),
	}),
});

export const {
	useGetRecipeByIdQuery,
	useGetRecipesQuery,
	useAddRecipeMutation,
	useDeleteRecipeMutation,
	useUpdateRecipeMutation,
} = api;
