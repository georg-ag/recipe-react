import React, { useState } from 'react';
import {
	Box,
	Card,
	CardActionArea,
	CardActions,
	CardContent,
	CardHeader,
	CardMedia,
	Collapse,
	Divider,
	IconButton,
	IconButtonProps,
	List,
	ListItem,
	ListItemText,
	Menu,
	MenuItem,
	Typography,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';

import { useAppDispatch } from '../../../app/hooks';
import Recipe from '../../../shared/models/Recipe';
import { push } from 'connected-react-router';
import Ingredient from '../../../shared/models/Ingredient';
import DeleteDialog from './DeleteDialog';

interface ExpandMoreProps extends IconButtonProps {
	expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
	const { expand, ...other } = props;
	return <IconButton {...other} size="large" />;
})(({ theme, expand }) => ({
	transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
	marginLeft: 'auto',
	transition: theme.transitions.create('transform', {
		duration: theme.transitions.duration.shortest,
	}),
}));

export default function RecipeCard({ recipe }: { recipe: Recipe }) {
	const [ingredientsExpanded, setIngredientsExpanded] = useState(false);
	const [menuAnchorEl, setMenuAnchorEl] = useState<null | HTMLElement>(null);
	const [dialogOpen, setDialogOpen] = useState(false);

	const menuOpen = Boolean(menuAnchorEl);

	const handleMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
		setMenuAnchorEl(event.currentTarget);
	};
	const handleMenuClose = () => {
		setMenuAnchorEl(null);
	};

	const dispatch = useAppDispatch();

	const handleOpenRecipe = () => {
		dispatch(push(`/recipes/${recipe.id}`));
	};

	const handleEdit = () => {
		handleMenuClose();
		dispatch(push(`/recipes/${recipe.id}/edit`));
	};

	const handleDelete = () => {
		setDialogOpen(true);
	};

	const handleDialogClose = () => {
		setDialogOpen(false);
		handleMenuClose();
	};

	const handleExpandIngredients = () => {
		setIngredientsExpanded(!ingredientsExpanded);
	};

	const getListItemText = (ingredient: Ingredient) => {
		if (ingredient.quantity > 0) {
			return `${ingredient.name}: ${ingredient.quantity} ${ingredient.unit}`;
		} else {
			return ingredient.name;
		}
	};

	return (
		<Card sx={{ maxWidth: 345 }}>
			<CardActionArea onClick={handleOpenRecipe}>
				<CardHeader title={recipe.title} />
				<CardMedia
					sx={{ objectFit: 'cover', width: '100%', height: 200 }}
					component="img"
					src={recipe.imageUrl}
				/>
				<CardContent>
					<Typography variant="body2" color="text.secondary" component="p">
						{recipe.description}
					</Typography>
				</CardContent>
			</CardActionArea>
			<CardActions disableSpacing>
				<IconButton aria-label="add to favorites" size="large">
					<FavoriteIcon />
				</IconButton>
				<IconButton aria-label="share" size="large">
					<ShareIcon />
				</IconButton>
				<IconButton id="options-button" aria-label="options" size="large" onClick={handleMenuClick}>
					<MoreVertIcon />
				</IconButton>
				<Menu
					id="context-menu"
					anchorEl={menuAnchorEl}
					open={menuOpen}
					onClose={handleMenuClose}
					MenuListProps={{ 'aria-labelledby': 'options-button' }}
				>
					<MenuItem aria-label="edit" onClick={handleEdit}>
						Edit
					</MenuItem>
					<MenuItem aria-label="delete" onClick={handleDelete}>
						Delete
					</MenuItem>
					<DeleteDialog open={dialogOpen} onClose={handleDialogClose} recipeId={recipe.id} />
				</Menu>
				<ExpandMore
					expand={ingredientsExpanded}
					onClick={handleExpandIngredients}
					aria-expanded={ingredientsExpanded}
					aria-label="show more"
					size="large"
				>
					<ExpandMoreIcon />
				</ExpandMore>
			</CardActions>
			<Collapse in={ingredientsExpanded} timeout="auto" unmountOnExit>
				<CardContent>
					<Typography paragraph variant="h6">
						Ingredients:
					</Typography>
					<List>
						<Divider />
						{recipe.ingredients.map((ingredient) => {
							return (
								<Box key={ingredient.id}>
									<ListItem>
										<ListItemText primary={getListItemText(ingredient)} />
									</ListItem>
									<Divider />
								</Box>
							);
						})}
					</List>
				</CardContent>
			</Collapse>
		</Card>
	);
}
