import React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useAppDispatch } from '../../../app/hooks';
import { useDeleteRecipeMutation } from '../../api/api';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { push } from 'connected-react-router';

export default function DeleteDialog({
	open,
	onClose,
	recipeId,
}: {
	open: boolean;
	onClose: () => void;
	recipeId: string;
}) {
	const dispatch = useAppDispatch();
	const [deleteRecipe, { isLoading: isDeleting }] = useDeleteRecipeMutation();

	const handleDelete = () => {
		onClose();
		deleteRecipe(recipeId)
			.unwrap()
			.then(() => {
				dispatch(push('/recipes'));
			});
	};

	return (
		<>
			<Dialog
				open={open}
				onClose={onClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogTitle id="alert-dialog-title">{'Delete Recipe?'}</DialogTitle>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">
						This action cannot be undone.
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleDelete}>Delete</Button>
					<Button onClick={onClose} autoFocus>
						Cancel
					</Button>
				</DialogActions>
			</Dialog>
			<Backdrop
				sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
				open={isDeleting}
			>
				<CircularProgress color="inherit" />
			</Backdrop>
		</>
	);
}
