import React, { useEffect, useState } from 'react';
import {
	Box,
	Button,
	Card,
	CardContent,
	CardMedia,
	List,
	TextField,
	Typography,
} from '@mui/material';

import { useAppSelector } from '../../../app/hooks';
import IngredientForm from './IngredientForm';
import Recipe from '../../../shared/models/Recipe';
import { Prompt } from 'react-router-dom';
import useExitPrompt from '../../../components/useExitPrompt';

export default function RecipeForm({
	onSave,
	onCancel,
}: {
	onSave: (recipe: Recipe) => void;
	onCancel: () => void;
}) {
	const originalRecipe = useAppSelector((state) => state.recipe.editedRecipe);
	const hasIngredientChanged = useAppSelector((state) => state.recipe.hasIngredientChanged);
	const ingredients = useAppSelector((state) => state.recipe.editedRecipe.ingredients);
	const ingredientIds = useAppSelector((state) =>
		state.recipe.editedRecipe.ingredients.map((ingredient) => ingredient.id),
	);

	const [title, setTitle] = useState(originalRecipe.title);
	const [description, setDescription] = useState(originalRecipe.description);
	const [imageUrl, setImageUrl] = useState(originalRecipe.imageUrl);
	const [instructions, setInstructions] = useState(originalRecipe.instructions);

	const [isSaved, setIsSaved] = useState(false);

	const hasUnsavedChanges = () => {
		if (isSaved) {
			return false;
		} else {
			return (
				title !== originalRecipe.title ||
				description !== originalRecipe.description ||
				imageUrl !== originalRecipe.imageUrl ||
				instructions !== originalRecipe.instructions ||
				hasIngredientChanged
			);
		}
	};

	const [showExitPrompt, setShowExitPrompt] = useExitPrompt(false);
	useEffect(() => {
		return () => {
			setShowExitPrompt(false);
		};
	}, []);

	const getEmptyForm = () => {
		var newId = 0;
		if (ingredientIds.length > 0) {
			newId = Math.max(...ingredientIds) + 1;
		}
		return <IngredientForm id={newId} key={newId} isNewForm={true} />;
	};

	const renderedIngredients = ingredientIds.map((ingredientId) => {
		return <IngredientForm id={ingredientId} key={ingredientId} isNewForm={false} />;
	});

	const onTitleChanged = (e: { target: { value: string } }) => setTitle(e.target.value);
	const onDescriptionChanged = (e: { target: { value: string } }) => setDescription(e.target.value);
	const onImageUrlChanged = (e: { target: { value: string } }) => setImageUrl(e.target.value);
	const onInstructionsChanged = (e: { target: { value: string } }) =>
		setInstructions(e.target.value);

	useEffect(() => {
		setShowExitPrompt(hasUnsavedChanges());
	});

	const onSaveButtonClicked = () => {
		const savedRecipe = {
			...originalRecipe,
			title,
			description,
			imageUrl,
			instructions,
			ingredients,
		};
		onSave(savedRecipe);
		setIsSaved(true);
	};

	const isValidUrl = () => {
		try {
			new URL(imageUrl);
		} catch (e) {
			return false;
		}
		return true;
	};

	return (
		<Box
			component="form"
			sx={{
				'& .MuiTextField-root': { m: 1 },
				ml: '20px',
			}}
			noValidate
			autoComplete="off"
		>
			<div>
				<TextField required id="title" label="Title" value={title} onChange={onTitleChanged} />
				<TextField
					id="description"
					label="Description"
					value={description}
					onChange={onDescriptionChanged}
				/>
				<Box>
					<Card sx={{ maxWidth: 345, ml: '8px' }}>
						{isValidUrl() ? (
							<CardMedia
								sx={{
									objectFit: 'cover',
									minHeight: { xs: 200, lg: 100 },
								}}
								component="img"
								src={imageUrl}
								alt="recipe image"
							/>
						) : (
							<CardContent
								sx={{
									objectFit: 'cover',
									minHeight: { xs: 200, lg: 100 },
								}}
							>
								<Typography>Invalid URL</Typography>
							</CardContent>
						)}
					</Card>
					<TextField
						id="image-url"
						label="Image URL"
						value={imageUrl}
						onChange={onImageUrlChanged}
						sx={{ width: '50em' }}
					/>
				</Box>
				<br />
				<Typography variant="h6" sx={{ ml: '8px' }}>
					Ingredients:
				</Typography>
				<List sx={{ flexGrow: 1, width: { lg: 1000 } }}>
					{renderedIngredients}
					{getEmptyForm()}
				</List>
				<TextField
					onChange={onInstructionsChanged}
					required
					id="instructions"
					label="Instructions"
					value={instructions}
					multiline
					fullWidth
					minRows={5}
					maxRows={Infinity}
				/>
			</div>
			<Box>
				<Button
					onClick={onCancel}
					variant="contained"
					color="error"
					sx={{ float: 'right', fontSize: 20, margin: 2 }}
				>
					Cancel
				</Button>
				<Button
					onClick={onSaveButtonClicked}
					variant="contained"
					color="success"
					sx={{ float: 'right', fontSize: 20, margin: 2 }}
				>
					Save
				</Button>
			</Box>
			<Prompt
				when={showExitPrompt}
				message='Are you sure you want to leave? Unsaved changes will be lost if you click "OK".'
			/>
		</Box>
	);
}
