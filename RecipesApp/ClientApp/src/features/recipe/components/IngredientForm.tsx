import React, { useEffect, useState } from 'react';

import { Box, ListItem, IconButton, TextField } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';

import { useAppDispatch, useAppSelector } from '../../../app/hooks';
import { RootState } from '../../../app/store';
import {
	ingredientAdded,
	ingredientChanged,
	ingredientDeleted,
	rememberFocus,
} from '../recipeSlice';

const selectIngredientById = (state: RootState, ingredientId: number) => {
	const ingredient = state.recipe.editedRecipe.ingredients.find(
		(listItem) => listItem.id === ingredientId,
	);
	return ingredient
		? ingredient
		: { id: ingredientId, name: '', details: '', quantity: 0, unit: '' };
};

export default function IngredientForm({ id, isNewForm }: { id: number; isNewForm: boolean }) {
	const dispatch = useAppDispatch();
	const [isNew, setIsNew] = useState(isNewForm);

	const ingredient = useAppSelector((state) => selectIngredientById(state, id));
	const focusedInput = useAppSelector((state) => state.recipe.focusedInput);
	const nameField = React.useRef<HTMLDivElement>(null);
	const detailsField = React.useRef<HTMLDivElement>(null);
	const quantityField = React.useRef<HTMLDivElement>(null);
	const unitField = React.useRef<HTMLDivElement>(null);

	const [name, setName] = useState(ingredient.name);
	const [details, setDetails] = useState(ingredient.details);
	const [quantity, setQuantity] = useState(ingredient.quantity);
	const [unit, setUnit] = useState(ingredient.unit);
	const isChanged = () => {
		if (
			name === ingredient.name &&
			details === ingredient.details &&
			quantity === ingredient.quantity &&
			unit === ingredient.unit
		) {
			return false;
		}
		return true;
	};

	const onNameChanged = (e: { target: { value: string } }) => {
		setName(e.target.value);
	};

	const onDetailsChanged = (e: { target: { value: string } }) => {
		setDetails(e.target.value);
	};

	const onQuantityChanged = (e: { target: { value: string } }) => {
		const newQuantity = parseFloat(e.target.value);
		setQuantity(newQuantity ? newQuantity : 0);
	};

	const onUnitChanged = (e: { target: { value: string } }) => {
		setUnit(e.target.value);
	};

	const handleBlur = (e: any) => {
		if (isChanged()) {
			if (isNew) {
				dispatch(ingredientAdded({ id, name, details, quantity, unit }));
				setIsNew(false);
				if (e.currentTarget.contains(e.relatedTarget)) {
					dispatch(rememberFocus(e.relatedTarget.name));
					console.log(focusedInput);
				}
			} else {
				dispatch(ingredientChanged({ id, name, details, quantity, unit }));
			}
		}
	};

	const onDeleteClicked = () => {
		dispatch(ingredientDeleted(id));
	};

	const focusNameInput = () => {
		if (nameField?.current) {
			(nameField.current.children[1].children[0] as HTMLInputElement).focus();
		}
	};

	const focusDetailsInput = () => {
		if (detailsField?.current) {
			(detailsField.current.children[1].children[0] as HTMLInputElement).focus();
		}
	};

	const focusQuantityInput = () => {
		if (quantityField?.current) {
			(quantityField.current.children[1].children[0] as HTMLInputElement).focus();
		}
	};

	const focusUnitInput = () => {
		if (unitField?.current) {
			(unitField.current.children[1].children[0] as HTMLInputElement).focus();
		}
	};

	useEffect(() => {
		if (!isNew && !!focusedInput && focusedInput !== '') {
			switch (focusedInput) {
				case 'name':
					focusNameInput();
					break;
				case 'details':
					focusDetailsInput();
					break;
				case 'quantity':
					focusQuantityInput();
					break;
				case 'unit':
					focusUnitInput();
					break;
				default:
					break;
			}
			dispatch(rememberFocus(''));
		}
	}, []);

	return (
		<Box onBlur={handleBlur}>
			<ListItem
				secondaryAction={
					!isNewForm && (
						<IconButton onClick={onDeleteClicked}>
							<DeleteIcon />
						</IconButton>
					)
				}
			>
				<TextField
					value={name}
					label="Ingredient"
					id="ingredient"
					onChange={onNameChanged}
					ref={nameField}
					name="name"
				/>
				<TextField
					value={details}
					label="Details"
					id="details"
					onChange={onDetailsChanged}
					ref={detailsField}
					name="details"
				/>
				<TextField
					value={quantity > 0 ? quantity : ''}
					label="Amount"
					id="amount"
					type="number"
					InputProps={{
						inputProps: {
							min: 0.0,
						},
					}}
					onChange={onQuantityChanged}
					ref={quantityField}
					name="quantity"
				/>
				<TextField
					value={unit}
					label="Unit"
					id="unit"
					onChange={onUnitChanged}
					ref={unitField}
					name="unit"
				/>
			</ListItem>
		</Box>
	);
}
