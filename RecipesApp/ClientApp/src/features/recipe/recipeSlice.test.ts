import Recipe from '../../shared/models/Recipe';
import reducer, {
	editStarted,
	ingredientAdded,
	ingredientChanged,
	ingredientDeleted,
	rememberFocus,
} from './recipeSlice';

const initialState = {
	editedRecipe: {
		id: '',
		title: '',
		description: '',
		imageUrl: '',
		instructions: '',
		ingredients: [],
		createdTimeStamp: '',
		changedTimeStamp: '',
		isShared: false,
		userId: '',
	},
	focusedInput: '',
	hasIngredientChanged: false,
};

const dummyRecipe: Recipe = {
	id: 'guid',
	title: 'Dummy Recipe',
	description: 'Just for testing',
	imageUrl: 'url.fake',
	ingredients: [{ id: 0, name: 'Carrot', details: 'for rabbit', quantity: 10, unit: '' }],
	instructions: 'Test the recipe.',
	changedTimeStamp: 'yesterday',
	createdTimeStamp: 'lastWeek',
	isShared: false,
	userId: 'userGuid',
};

test('returns the inital state', () =>
	expect(reducer(undefined, { type: 'undefined' })).toEqual({
		editedRecipe: {
			id: '',
			title: '',
			description: '',
			imageUrl: '',
			instructions: '',
			ingredients: [],
			createdTimeStamp: '',
			changedTimeStamp: '',
			isShared: false,
			userId: '',
		},
		focusedInput: '',
		hasIngredientChanged: false,
	}));

test('handles a recipe being edited', () => {
	const previousState = initialState;
	expect(reducer(previousState, editStarted(dummyRecipe))).toEqual({
		...previousState,
		editedRecipe: dummyRecipe,
	});
});

test('handles an ingredient being changed', () => {
	const previousState = { ...initialState, editedRecipe: dummyRecipe };
	const changedIngredient = { id: 0, name: 'Moehre', details: 'orange', quantity: 12, unit: 'g' };
	expect(reducer(previousState, ingredientChanged(changedIngredient))).toEqual({
		...previousState,
		editedRecipe: { ...dummyRecipe, ingredients: [changedIngredient] },
		hasIngredientChanged: true,
	});
});

test('ignores an invalid changed ingredient', () => {
	const previousState = { ...initialState, editedRecipe: dummyRecipe };
	const changedIngredient = { id: 1, name: 'Moehre', details: 'orange', quantity: 12, unit: 'g' };
	expect(reducer(previousState, ingredientChanged(changedIngredient))).toEqual(previousState);
});

test('handles an ingredient being added', () => {
	const previousState = initialState;
	const newIngredient = { id: 1, name: 'Moehre', details: 'orange', quantity: 12, unit: 'g' };
	expect(reducer(previousState, ingredientAdded(newIngredient))).toEqual({
		...previousState,
		editedRecipe: { ...previousState.editedRecipe, ingredients: [newIngredient] },
		hasIngredientChanged: true,
	});
});

test('ignores an invalid ingredient being added', () => {
	const previousState = { ...initialState, editedRecipe: dummyRecipe };
	const newIngredient = { id: 0, name: 'Moehre', details: 'orange', quantity: 12, unit: 'g' };
	expect(reducer(previousState, ingredientAdded(newIngredient))).toEqual(previousState);
});

test('handles an ingredient being deleted', () => {
	const previousState = { ...initialState, editedRecipe: dummyRecipe };
	expect(reducer(previousState, ingredientDeleted(0))).toEqual({
		...previousState,
		editedRecipe: { ...dummyRecipe, ingredients: [] },
		hasIngredientChanged: true,
	});
});

test('remembers a focused input', () => {
	const previousState = initialState;
	expect(reducer(previousState, rememberFocus('input name'))).toEqual({
		...previousState,
		focusedInput: 'input name',
	});
});
