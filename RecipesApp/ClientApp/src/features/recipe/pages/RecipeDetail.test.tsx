import React from 'react';
import { setupServer } from 'msw/node';
import { render, screen, waitForElementToBeRemoved } from '../../../utils/test-utils';
import { handlers } from '../../../mocks/handlers';
import { mockRecipes } from '../../../mocks/recipes';
import RecipeDetail from './RecipeDetail';
import userEvent from '@testing-library/user-event';

const server = setupServer(...handlers);
const recipe = mockRecipes[0];

beforeAll(async () => server.listen());

beforeEach(async () => {
	render(<RecipeDetail />);
	await screen.findByText(recipe.instructions);
});

afterEach(() => server.resetHandlers());

afterAll(() => server.close());

test('renders instructions', async () => {
	expect(screen.getByText(recipe.instructions)).toBeInTheDocument();
});

test('renders ingredients', async () => {
	recipe.ingredients.forEach((ingredient) => {
		expect(screen.getByText(ingredient.name, { exact: false })).toBeInTheDocument();
		if (ingredient.details) {
			expect(screen.getByText(ingredient.details, { exact: false })).toBeInTheDocument();
		}
	});
});

// Deleting
describe('The delete option', () => {
	beforeEach(() => {
		const deleteButton = screen.getByRole('button', { name: /delete/i });
		userEvent.click(deleteButton);
	});

	test('opens the dialog', () => {
		expect(screen.getByRole('heading', { name: /delete recipe\?/i })).toBeInTheDocument();
	});

	test('closes the dialog on cancel', async () => {
		const cancelButton = screen.getByRole('button', { name: /cancel/i });
		userEvent.click(cancelButton);
		await waitForElementToBeRemoved(() =>
			screen.queryByRole('heading', { name: /delete recipe\?/i }),
		);
	});

	test('closes the dialog on delete', async () => {
		const deleteButton = screen.getByRole('button', { name: /delete/i });
		userEvent.click(deleteButton);
		await waitForElementToBeRemoved(() =>
			screen.queryByRole('heading', { name: /delete recipe\?/i }),
		);
	});
});
