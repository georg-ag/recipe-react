import React from 'react';
import { setupServer } from 'msw/node';
import { render, screen } from '../../../utils/test-utils';
import userEvent from '@testing-library/user-event';
import { handlers } from '../../../mocks/handlers';
import { mockRecipes } from '../../../mocks/recipes';
import RecipeOverview from './RecipeOverview';

const server = setupServer(...handlers);

beforeAll(() => server.listen());

afterEach(() => server.resetHandlers());

afterAll(() => server.close());

test('renders progressbar, while loading', () => {
	render(<RecipeOverview />);
	expect(screen.getByRole('progressbar', { hidden: true }));
});

test('renders add button', () => {
	render(<RecipeOverview />);
	expect(screen.getByLabelText(/add/i)).toBeInTheDocument();
});

test('renders recipes', async () => {
	render(<RecipeOverview />);
	await screen.findByText(mockRecipes[0].title);
	mockRecipes.forEach((recipe) => {
		expect(screen.getByText(recipe.title)).toBeInTheDocument();
	});
});

test('expands ingredients', async () => {
	render(<RecipeOverview />);
	await screen.findByText(mockRecipes[0].title);
	userEvent.click(screen.getAllByLabelText('show more')[0]);
	expect(await screen.findByText(/Ingredients/i)).toBeInTheDocument();
});

test('minimizes ingredients', async () => {
	render(<RecipeOverview />);
	await screen.findByText(mockRecipes[0].title);
	const el = screen.getAllByLabelText('show more')[0];
	userEvent.click(el);
	userEvent.click(el);
	expect(await screen.findByText(/Ingredients/i)).not.toBeInTheDocument();
});
