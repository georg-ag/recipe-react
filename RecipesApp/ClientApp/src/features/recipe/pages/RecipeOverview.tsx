import React from 'react';

import { Backdrop, Box, CircularProgress, Fab, Grid } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

import RecipeCard from '../components/RecipeCard';
import { useGetRecipesQuery } from '../../api/api';
import { push } from 'connected-react-router';
import { useAppDispatch } from '../../../app/hooks';

export default function RecipeOverview() {
	const { data = [], isFetching } = useGetRecipesQuery();
	const dispatch = useAppDispatch();

	const handleAdd = () => {
		dispatch(push(`/recipes/new`));
	};

	return (
		<Box sx={{ flexGrow: 1 }}>
			<Grid container spacing={3}>
				{data.map((recipe) => {
					return (
						<Grid key={recipe.id} item md={4} xl={3}>
							<RecipeCard recipe={recipe} />
						</Grid>
					);
				})}
			</Grid>
			<Fab
				aria-label="add"
				color="secondary"
				onClick={handleAdd}
				sx={{
					margin: 0,
					top: 'auto',
					right: 20,
					bottom: 20,
					left: 'auto',
					position: 'fixed',
				}}
			>
				<AddIcon />
			</Fab>
			<Backdrop
				sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
				open={isFetching}
			>
				<CircularProgress color="inherit" />
			</Backdrop>
		</Box>
	);
}
