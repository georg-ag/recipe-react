import React, { useEffect } from 'react';
import { Backdrop, CircularProgress } from '@mui/material';
import { useAppDispatch } from '../../../app/hooks';
import RecipeForm from '../components/RecipeForm';
import { editStarted } from '../recipeSlice';
import Recipe, { getEmptyRecipe } from '../../../shared/models/Recipe';
import { useAddRecipeMutation } from '../../api/api';
import { push } from 'connected-react-router';

export default function RecipeCreation() {
	const dispatch = useAppDispatch();
	dispatch(editStarted(getEmptyRecipe()));
	const [addRecipe, { isLoading: isUpdating, data }] = useAddRecipeMutation();

	const onSave = (recipe: Recipe) => {
		addRecipe(recipe);
	};

	useEffect(() => {
		if (!!data) {
			dispatch(push(`/recipes/${data.id}`));
		}
	}, [data]);

	const onCancel = () => {
		dispatch(push('/recipes'));
	};

	return (
		<>
			<RecipeForm onSave={onSave} onCancel={onCancel} />
			<Backdrop
				sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
				open={isUpdating}
			>
				<CircularProgress color="inherit" />
			</Backdrop>
		</>
	);
}
