import React from 'react';
import { setupServer } from 'msw/node';
import { render, screen } from '../../../utils/test-utils';
import { handlers } from '../../../mocks/handlers';
import { mockRecipes } from '../../../mocks/recipes';
import RecipeCreation from './RecipeCreation';

const server = setupServer(...handlers);
const recipe = mockRecipes[0];

beforeAll(async () => server.listen());

afterEach(() => server.resetHandlers());

afterAll(() => server.close());

test('renders empty form', async () => {
	render(<RecipeCreation />);
	const inputs: HTMLInputElement[] = await screen.findAllByRole('textbox');
	inputs.forEach((input) => {
		expect(input.value).toEqual('');
	});
});
