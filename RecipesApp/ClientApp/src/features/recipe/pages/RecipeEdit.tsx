import React from 'react';
import { useAppDispatch } from '../../../app/hooks';
import { Backdrop, CircularProgress } from '@mui/material';
import { connect } from 'react-redux';
import Recipe from '../../../shared/models/Recipe';
import { useGetRecipeByIdQuery, useUpdateRecipeMutation } from '../../api/api';
import RecipeForm from '../components/RecipeForm';
import FormSkeleton from '../skeletons/FormSkeleton';
import { editStarted } from '../recipeSlice';
import { push } from 'connected-react-router';

const RecipeEdit = ({ pathname }: { pathname: string }) => {
	const dispatch = useAppDispatch();
	const frontRemoved = pathname.split('recipes/').pop();
	const id = frontRemoved ? frontRemoved.split('/edit')[0] : '0';
	const { data = null as unknown as Recipe, isFetching } = useGetRecipeByIdQuery(id);
	dispatch(editStarted(data));
	const [updateRecipe, { isLoading: isUpdating }] = useUpdateRecipeMutation();

	const onSave = (recipe: Recipe) => {
		updateRecipe(recipe)
			.unwrap()
			.then(() => {
				dispatch(push(`/recipes/${id}`));
			});
	};

	const onCancel = () => {
		dispatch(push(`/recipes/${id}`));
	};

	return !isFetching ? (
		<>
			<RecipeForm onSave={onSave} onCancel={onCancel} />
			<Backdrop
				sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
				open={isUpdating}
			>
				<CircularProgress color="inherit" />
			</Backdrop>
		</>
	) : (
		<FormSkeleton />
	);
};

const mapStateToProps = (state: { router: { location: { pathname: any } } }) => ({
	pathname: state.router.location.pathname,
});

export default connect(mapStateToProps)(RecipeEdit);
