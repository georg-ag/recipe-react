import React from 'react';

import {
	Box,
	Button,
	Card,
	CardContent,
	CardMedia,
	Container,
	Divider,
	Grid,
	List,
	ListItem,
	ListItemText,
	Typography,
} from '@mui/material';

import { useGetRecipeByIdQuery } from '../../api/api';
import { connect } from 'react-redux';
import Recipe from '../../../shared/models/Recipe';
import DetailSkeleton from '../skeletons/DetailSkeleton';
import Ingredient from '../../../shared/models/Ingredient';
import DeleteDialog from '../components/DeleteDialog';
import { useAppDispatch } from '../../../app/hooks';
import { push } from 'connected-react-router';

const RecipeDetail = ({ pathname }: { pathname: string }) => {
	const [dialogOpen, setDialogOpen] = React.useState(false);
	const dispatch = useAppDispatch();
	const id = pathname.split('recipes/')[1];
	const { data = null as unknown as Recipe, isFetching } = useGetRecipeByIdQuery(id);
	const recipe = data;

	const getListItemText = (ingredient: Ingredient) => {
		let ingredientText = ingredient.name;
		if (ingredient.details) {
			ingredientText = ingredientText.concat(` (${ingredient.details})`);
		}
		if (ingredient.quantity > 0) {
			ingredientText = ingredientText.concat(`: ${ingredient.quantity} ${ingredient.unit}`);
		}
		return ingredientText;
	};

	return !isFetching ? (
		<>
			<Grid container spacing={0} sx={{ flexGrow: 1 }}>
				<Grid item xs={12} lg={2}>
					<Box
						sx={{
							height: '100%',
							display: 'flex',
							flexDirection: 'column',
							position: { lg: 'fixed' },
							width: { lg: '17%' },
						}}
					>
						<Card>
							<CardMedia
								sx={{
									objectFit: 'cover',
									minHeight: { xs: 200, lg: 100 },
								}}
								component="img"
								src={recipe.imageUrl}
							/>
							<CardContent>
								<Typography paragraph variant="h6">
									Ingredients:
								</Typography>
								<List>
									<Divider />
									{recipe.ingredients.map((ingredient) => {
										return (
											<Box key={ingredient.id}>
												<ListItem>
													<ListItemText primary={getListItemText(ingredient)} />
												</ListItem>
												<Divider />
											</Box>
										);
									})}
								</List>
							</CardContent>
						</Card>
					</Box>
				</Grid>
				<Grid item xs={12} lg={8}>
					<Container>
						<Typography variant="h2" sx={{ textAlign: 'center' }}>
							{recipe.title}
						</Typography>
						<Divider />
						<Typography variant="subtitle1" sx={{ textAlign: 'center' }}>
							{recipe.description}
						</Typography>
						<Typography
							sx={{
								whiteSpace: 'pre-line',
								textAlign: 'left',
								mt: '2vh',
							}}
							variant="body1"
						>
							{recipe.instructions}
						</Typography>
					</Container>
				</Grid>
			</Grid>
			<Box
				sx={{
					margin: 0,
					top: 'auto',
					right: 20,
					bottom: 20,
					left: 'auto',
					position: 'fixed',
				}}
			>
				<Button
					onClick={() => dispatch(push(`/recipes/${recipe.id}/edit`))}
					variant="contained"
					color="info"
					sx={{ fontSize: 20, margin: 2 }}
				>
					Edit
				</Button>
				<Button
					onClick={() => setDialogOpen(true)}
					variant="contained"
					color="error"
					sx={{ float: 'right', fontSize: 20, margin: 2 }}
				>
					Delete
				</Button>
				<DeleteDialog open={dialogOpen} onClose={() => setDialogOpen(false)} recipeId={recipe.id} />
			</Box>
		</>
	) : (
		<DetailSkeleton />
	);
};

const mapStateToProps = (state: { router: { location: { pathname: any } } }) => ({
	pathname: state.router.location.pathname,
});

export default connect(mapStateToProps)(RecipeDetail);
