import React from 'react';
import { setupServer } from 'msw/node';
import { render, screen, within } from '../../../utils/test-utils';
import { handlers } from '../../../mocks/handlers';
import { mockRecipes } from '../../../mocks/recipes';
import RecipeEdit from './RecipeEdit';
import App from '../../../App';
import userEvent from '@testing-library/user-event';

const server = setupServer(...handlers);
const recipe = mockRecipes[0];

beforeAll(async () => {
	server.listen();
	render(<App />);
	const el = await screen.findByText(recipe.title);
	userEvent.click(el);
});

beforeEach(async () => {
	render(<RecipeEdit />);
	await screen.findAllByText(/Title \*/i);
});

afterEach(() => server.resetHandlers());

afterAll(() => server.close());

test('renders description input', async () => {
	const descriptionInput: HTMLInputElement = screen.getByLabelText(/Description/i);
	expect(descriptionInput.value).toEqual(recipe.description);
});

test('renders title input', async () => {
	const descriptionInput: HTMLInputElement = screen.getByLabelText(/Title/i);
	expect(descriptionInput.value).toEqual(recipe.title);
});

test('renders image url input', async () => {
	const urlInput: HTMLInputElement = screen.getByLabelText(/Image URL/i);
	expect(urlInput.value).toEqual(recipe.imageUrl);
});

test('renders instructions input', async () => {
	const instructionsInput: HTMLInputElement = screen.getByLabelText(/Instructions/i);
	expect(instructionsInput.value).toEqual(recipe.instructions);
});

test('renders ingredient input', async () => {
	recipe.ingredients.forEach((ingredient) => {
		expect(screen.getByDisplayValue(ingredient.name)).toBeInTheDocument();
	});
});

test('adds ingredient form', async () => {
	const list = screen.getByRole('list');
	const inputs = within(list).getAllByRole('textbox');
	const ingredientCount = inputs.length / 3 - 1;
	expect(ingredientCount).toEqual(recipe.ingredients.length);
	userEvent.click(inputs[inputs.length - 1]);
	userEvent.keyboard('test');
	userEvent.click(inputs[inputs.length - 3]);
	const newCount = within(list).getAllByRole('textbox').length / 3 - 1;
	expect(newCount).toBe(ingredientCount + 1);
});

test('detects changes to ingredients', async () => {
	const list = screen.getByRole('list');
	const inputs = within(list).getAllByRole('textbox').slice(0, 4) as HTMLTextAreaElement[];
	inputs.forEach((input) => {
		const oldValue = input.value;
		userEvent.click(input);
		userEvent.keyboard('1');
		userEvent.tab();
		expect(input.value).toEqual(oldValue + '1');
	});
});
