import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import Ingredient from '../../shared/models/Ingredient';

import Recipe, { getEmptyRecipe } from '../../shared/models/Recipe';
interface RecipeState {
	editedRecipe: Recipe;
	focusedInput: string;
	hasIngredientChanged: boolean;
}

const initialState: RecipeState = {
	editedRecipe: getEmptyRecipe(),
	focusedInput: '',
	hasIngredientChanged: false,
};

const recipeSlice = createSlice({
	name: 'recipes',
	initialState,
	reducers: {
		editStarted(state, action: PayloadAction<Recipe>) {
			state.editedRecipe = action.payload;
			state.hasIngredientChanged = false;
		},
		ingredientChanged(state, action: PayloadAction<Ingredient>) {
			const index = state.editedRecipe.ingredients.findIndex(
				(ingredient) => ingredient.id === action.payload.id,
			);
			if (index >= 0) {
				state.editedRecipe.ingredients[index] = action.payload;
				state.hasIngredientChanged = true;
			} else {
				console.log(`no ingredient with id ${action.payload.id}`);
			}
		},
		ingredientAdded(state, action: PayloadAction<Ingredient>) {
			const newIngredient = action.payload;
			if (
				!state.editedRecipe.ingredients.some((ingredient) => ingredient.id === newIngredient.id)
			) {
				state.editedRecipe.ingredients = [...state.editedRecipe.ingredients, action.payload];
				state.hasIngredientChanged = true;
			} else {
				console.log(`ingredient with id ${newIngredient.id} already exists`);
			}
		},
		ingredientDeleted(state, action: PayloadAction<number>) {
			state.editedRecipe.ingredients = state.editedRecipe.ingredients.filter(
				(ingredient) => ingredient.id !== action.payload,
			);
			state.hasIngredientChanged = true;
		},
		rememberFocus(state, action: PayloadAction<string>) {
			state.focusedInput = action.payload;
		},
	},
});

export const { editStarted, ingredientChanged, ingredientAdded, ingredientDeleted, rememberFocus } =
	recipeSlice.actions;
export default recipeSlice.reducer;
