import { Skeleton } from '@mui/material';
import React from 'react';

export default function FormSkeleton() {
  return <Skeleton />;
}
