import { Box, Container, Divider, Grid, Skeleton } from '@mui/material';
import React from 'react';

export default function DetailSkeleton() {
	const lineWidths = [64, 67,	24,	39,	38, 58,	34,	29,	49,	41, 53,	30,	55,	39,	31,	44,	32,	26, 66, 50, 62, 44, 61, 63, 43];
  return (
    <Grid container spacing={0} sx={{ flexGrow: 1 }}>
      <Grid item xs={12} lg={2}>
        <Box
          sx={{
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            position: { lg: 'fixed' },
            width: { lg: '17%' },
          }}
        >
          <Skeleton animation="wave" sx={{ height: '60%' }} />
        </Box>
      </Grid>
      <Grid item xs={12} lg={8}>
        <Container>
          <Skeleton
            variant="text"
            animation="wave"
            height={80}
            width={'20vw'}
            sx={{ margin: 'auto' }}
          />
          <Divider />
          <Skeleton
            variant="text"
            animation="wave"
            height={40}
            width={'20vw'}
            sx={{ margin: 'auto' }}
          />
          {lineWidths.map((value: number, index: number) => (
            <Skeleton
              animation="wave"
              key={index}
              variant="text"
              height={25}
              width={`${value}vw`}
            />
          ))}
        </Container>
      </Grid>
    </Grid>
  );
}
