import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import recipeReducer from '../features/recipe/recipeSlice';
import { api } from '../features/api/api';

export const history = createBrowserHistory();

export const store = configureStore({
  reducer: {
    recipe: recipeReducer,
    [api.reducerPath]: api.reducer,
    // necessary because of typing issue
    // @ts-ignore
    router: connectRouter(history),
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(routerMiddleware(history), api.middleware),
  devTools: process.env.NODE_ENV !== 'production',
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
