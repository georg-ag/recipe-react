import React from 'react';
import { AppBar, Toolbar, IconButton, Typography, Button, Box, ButtonBase } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import { useAppDispatch } from '../../app/hooks';
import { push } from 'connected-react-router';

export default function Header() {
  const dispatch = useAppDispatch();
  const navigateToHome = () => {
    dispatch(push('/recipes'));
  };

  return (
    <Box sx={{ flexGrow: 1, mb: '10vh' }}>
      <AppBar sx={{ bgcolor: 'primary.main', position: 'fixed' }}>
        <Toolbar sx={{ justifyContent: 'space-between' }}>
          <IconButton size="large" edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }}>
            <MenuIcon />
          </IconButton>
          <ButtonBase centerRipple={false} disableRipple={false} disableTouchRipple={false}>
            <Typography variant="h6" onClick={navigateToHome}>
              Recipes
            </Typography>
          </ButtonBase>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
