import React from 'react';

import { Container } from '@mui/material';

import RecipeOverview from '../features/recipe/pages/RecipeOverview';

export default function Home() {
  return (
    <Container>
      <RecipeOverview />
    </Container>
  );
}
