import React from 'react';
import { setupServer } from 'msw/node';
import { render, screen, waitForElementToBeRemoved } from './utils/test-utils';
import { handlers } from './mocks/handlers';
import App from './App';
import { mockRecipes } from './mocks/recipes';
import userEvent from '@testing-library/user-event';

const server = setupServer(...handlers);

beforeAll(() => server.listen());

beforeEach(async () => {
	render(<App />);
	// navigate to overview
	const homeButton = await screen.findByRole('heading', { name: /recipes/i });
	userEvent.click(homeButton);
	await screen.findByText(mockRecipes[0].title);
});

afterEach(() => server.resetHandlers());

afterAll(() => server.close());

// Basic
test('renders header', () => {
	expect(screen.getByText(/Login/i)).toBeInTheDocument();
	expect(screen.getByText(/Recipes/i)).toBeInTheDocument();
});

test('renders overview', async () => {
	expect(await screen.findByText(mockRecipes[0].title)).toBeInTheDocument();
});

// Editing a recipe
describe('The edit view', () => {
	beforeEach(() => {
		const optionsButton = screen.getAllByRole('button', { name: 'options' })[0];
		userEvent.click(optionsButton);
		const editButton = screen.getByRole('menuitem', { name: /edit/i });
		userEvent.click(editButton);
	});

	test('is opened', async () => {
		expect((await screen.findAllByRole('textbox'))[0]).toBeInTheDocument();
	});

	test('navigates to details on save', async () => {
		const saveButton = await screen.findByRole('button', { name: /save/i });
		userEvent.click(saveButton);
		expect(
			await screen.findByRole('heading', { name: mockRecipes[0].description }),
		).toBeInTheDocument();
	});

	test('navigates to details on Cancel', async () => {
		const saveButton = await screen.findByRole('button', { name: /cancel/i });
		userEvent.click(saveButton);
		expect(
			await screen.findByRole('heading', { name: mockRecipes[0].description }),
		).toBeInTheDocument();
	});
});

// Creating a recipe
describe('The create view', () => {
	beforeEach(() => {
		const addButton = screen.getByRole('button', { name: 'add' });
		userEvent.click(addButton);
	});
	test('is opened', async () => {
		expect((await screen.findAllByRole('textbox'))[0]).toBeInTheDocument();
	});

	test('navigates to details on save', async () => {
		const descriptionInput = screen.getByRole('textbox', { name: /description/i });
		userEvent.click(descriptionInput);
		const description = 'test';
		userEvent.keyboard(description);
		const saveButton = screen.getByRole('button', { name: /save/i });
		userEvent.click(saveButton);
		expect(
			await screen.findByRole('heading', { name: mockRecipes[0].description }),
		).toBeInTheDocument();
	});

	test('navigates to overview on cancel', async () => {
		const saveButton = screen.getByRole('button', { name: /cancel/i });
		userEvent.click(saveButton);
		expect(await screen.findByText(mockRecipes[0].title)).toBeInTheDocument();
	});
});

// Deleting
describe('The delete option', () => {
	beforeEach(() => {
		const optionsButton = screen.getAllByRole('button', { name: 'options' })[0];
		userEvent.click(optionsButton);
		const deleteButton = screen.getByRole('menuitem', { name: /delete/i });
		userEvent.click(deleteButton);
	});

	test('opens the dialog', () => {
		expect(screen.getByRole('heading', { name: /delete recipe\?/i })).toBeInTheDocument();
	});

	test('closes the dialog on cancel', async () => {
		const cancelButton = screen.getByRole('button', { name: /cancel/i });
		userEvent.click(cancelButton);
		await waitForElementToBeRemoved(() =>
			screen.queryByRole('heading', { name: /delete recipe\?/i }),
		);
	});

	test('closes the dialog on delete', async () => {
		const deleteButton = screen.getByRole('button', { name: /delete/i });
		userEvent.click(deleteButton);
		await waitForElementToBeRemoved(() =>
			screen.queryByRole('heading', { name: /delete recipe\?/i }),
		);
	});
});
