using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace RecipesApp.Pages
{
    /// <summary>
    /// Abstract class for Errors
    /// </summary>
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class ErrorModel : PageModel
    {
        private readonly ILogger<ErrorModel> _logger;

        /// <summary>
        /// The constructor
        /// </summary>
        /// <param name="logger">An instance of a <see cref="ILogger"/></param>
        public ErrorModel(ILogger<ErrorModel> logger)
        {
            this._logger = logger;
        }

        /// <summary>
        /// The id of the request
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// whether or not the id should be shown
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);

        /// <summary>
        /// Gets the current i
        /// </summary>
        public void OnGet()
        {
            this.RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier;
        }
    }
}
