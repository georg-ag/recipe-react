using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RecipesApp.Recipes.Models;

namespace RecipesApp.Recipes.Services
{
    /// <inheritdoc />
    public class RecipeService : IRecipeService
    {
        /// <inheritdoc />
        public async Task<Recipe> AddRecipeAsync(CreateRecipe request)
        {
            var recipe = Recipe.fromRequest(request, GetUserId());
            TestData.Recipes.Add(recipe);
            await Task.Delay(100);
            return recipe;
        }

        /// <inheritdoc />
        public async Task DeleteRecipeAsync(Guid recipeId)
        {
            var trip = TestData.Recipes.Find(recipe => recipe.Id == recipeId);
            await Task.Delay(100);
            if (trip != null)
            {
                TestData.Recipes.Remove(trip);
            }
        }

        /// <inheritdoc />
        public async Task<Recipe> GetRecipeByIdAsync(Guid recipeId)
        {
            await Task.Delay(100);
            return TestData.Recipes.Find(recipe => recipe.Id == recipeId);
        }

        /// <inheritdoc />
        public async Task<List<Recipe>> GetSharedRecipesAsync()
        {
            await Task.Delay(100);
            return TestData.Recipes.FindAll(recipe => recipe.IsShared);
        }

        /// <inheritdoc />
        public async Task<List<Recipe>> GetUserRecipesAsync()
        {
            await Task.Delay(100);
            return TestData.Recipes.FindAll(recipe => recipe.UserId == GetUserId());
        }

        /// <inheritdoc />
        public async Task<Recipe> UpdateRecipeAsync(Guid recipeId, Recipe recipe)
        {
            var oldRecipe = TestData.Recipes.Find(recipe => recipe.Id == recipeId);
            await Task.Delay(100);
            if (oldRecipe != null)
            {
                oldRecipe.Title = recipe.Title;
                oldRecipe.Description = recipe.Description;
                oldRecipe.Instructions = recipe.Instructions;
                oldRecipe.ChangedTimeStamp = DateTimeOffset.Now;
                oldRecipe.ImageUrl = recipe.ImageUrl;
                oldRecipe.Ingredients = recipe.Ingredients;
                oldRecipe.IsShared = recipe.IsShared;
            }
            return oldRecipe;
        }

        private static Guid GetUserId() => new("8341a755-624d-4662-bf2e-cea74c793f59");
    }
}