using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RecipesApp.Recipes.Models;

namespace RecipesApp.Recipes.Services
{
    /// <summary>
    /// Interface defining methods for accessing and modifying a <see cref="Recipe"/>
    /// </summary>
    public interface IRecipeService
    {
        /// <summary>
        /// Gets all Recipes a user created
        /// </summary>
        /// <returns>A list of <see cref="Recipe"/></returns>
        Task<List<Recipe>> GetUserRecipesAsync();

        /// <summary>
        /// Gets all Recipes a user has shared
        /// </summary>
        /// <returns>A list of shared <see cref="Recipe"/></returns>
        Task<List<Recipe>> GetSharedRecipesAsync();

        /// <summary>
        /// Gets a single <see cref="Recipe"/> by its unique id
        /// </summary>
        /// <param name="recipeId">The <see cref="Guid"/> identifying the recipe</param>
        /// <returns>The <see cref="Recipe"/> with the Id</returns>
        Task<Recipe> GetRecipeByIdAsync(Guid recipeId);

        /// <summary>
        /// Adds a new <see cref="Recipe"/> to the users collection
        /// </summary>
        /// <param name="request">The <see cref="CreateRecipe"/> request containing information on the new recipe</param>
        /// <returns>The created <see cref="Recipe"/></returns>
        Task<Recipe> AddRecipeAsync(CreateRecipe request);

        /// <summary>
        /// Updates a <see cref="Recipe"/>
        /// </summary>
        /// <param name="recipeId">The <see cref="Guid"/> identifying the recipe</param>
        /// <param name="recipe">The updated <see cref="Recipe"/></param>
        /// <returns>The updated <see cref="Recipe"/></returns>
        Task<Recipe> UpdateRecipeAsync(Guid recipeId, Recipe recipe);

        /// <summary>
        /// Deletes a <see cref="Recipe"/>
        /// </summary>
        /// <param name="recipeId">The <see cref="Guid"/> identifying the recipe</param>
        /// <returns>A <see cref="Task"/> that can be awaited</returns>
        Task DeleteRecipeAsync(Guid recipeId);
    }
}