using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace RecipesApp.Recipes.Models
{
    /// <summary>
    /// Model for ingredients used in Recipes
    /// </summary>
    public class Ingredient
    {
        /// <summary>
        /// The Id of the Ingredient in a given recipe
        /// </summary>
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// The name of the ingredient
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Details about the ingredient e.g. fresh/dried
        /// </summary>
        public string Details { get; set; }

        /// <summary>
        /// The unit the ingredient is measured int
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// The amount that is required
        /// </summary>
        [Required]
        public double Quantity { get; set; }
    }
}
