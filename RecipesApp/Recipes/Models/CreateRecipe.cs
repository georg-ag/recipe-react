﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RecipesApp.Recipes.Models
{
    /// <summary>
    /// The model for received request changing/creating recipes
    /// </summary>
    public class CreateRecipe
    {
        /// <summary>
        /// The Title
        /// </summary>
        [Required]
        [StringLength(25, MinimumLength = 3, ErrorMessage="The Title has to be 3 to 25 Characters.")]
        public string Title { get; set; }

        /// <summary>
        /// The Description giving additional information
        /// </summary>
        [StringLength(25)]
        public string Description { get; set; }

        /// <summary>
        /// The instructions for preparing the meal
        /// </summary>
        public string Instructions { get; set; }

        /// <summary>
        /// A link to a title image
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The Ingredients used in the recipe
        /// </summary>
        public ICollection<Ingredient> Ingredients { get; set; }

        /// <summary>
        /// Defines if recipe is publicly visible
        /// </summary>
        [Required]
        public bool IsShared { get; set;  }
    }
}
