using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RecipesApp.Recipes.Models
{
    /// <summary>
    /// The model for Recipes stored in the Database
    /// </summary>
    public class Recipe
    {
        /// <summary>
        /// The unique identifier of a recipe
        /// </summary>
        [Required]
        public Guid Id { get; set; }

        /// <summary>
        /// The title
        /// </summary>
        [Required]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "The Title has to be 3 to 25 Characters.")]
        public string Title { get; set; }

        /// <summary>
        /// The description giving additional information
        /// </summary>
        [StringLength(25)]
        public string Description { get; set; }

        /// <summary>
        /// The instructions for preparing the meal
        /// </summary>
        public string Instructions { get; set; }

        /// <summary>
        /// A link to a title image
        /// </summary>
        [Url]
        public string ImageUrl { get; set; }

        /// <summary>
        /// The timestamp when the recipe was first created
        /// </summary>
        [Required]
        public DateTimeOffset? CreatedTimeStamp { get; set; }

        /// <summary>
        /// The timestamp when the recipe was last edited
        /// </summary>
        [Required]
        public DateTimeOffset? ChangedTimeStamp { get; set; }

        /// <summary>
        /// The Ingredients used in the recipe
        /// </summary>
        public ICollection<Ingredient> Ingredients { get; set; }

        /// <summary>
        /// Indicator whether the recipe can be found by other users
        /// </summary>
        [Required]
        public bool IsShared { get; set; }

        /// <summary>
        /// The unique identifier of the creator
        /// </summary>
        [Required]
        public Guid UserId { get; set; }

        /// <summary>
        /// default constructor
        /// </summary>
        public Recipe() { }

        private Recipe(string title, string description, string instructions, string imageUrl, ICollection<Ingredient> ingredients, bool isShared, Guid userId)
        {
            this.Id = Guid.NewGuid();
            this.Title = title;
            this.Description = description;
            this.Instructions = instructions;
            this.ImageUrl = imageUrl;
            this.CreatedTimeStamp = DateTimeOffset.Now;
            this.ChangedTimeStamp = DateTimeOffset.Now;
            this.Ingredients = ingredients;
            this.IsShared = isShared;
            this.UserId = userId;
        }

        /// <summary>
        /// Creates an instance of <see cref="Recipe"/> from a create request
        /// </summary>
        /// <param name="request">The incoming <see cref="CreateRecipe"/> request</param>
        /// <param name="userId">The unique id identifying the current user</param>
        /// <returns></returns>
        public static Recipe fromRequest(CreateRecipe request, Guid userId)
        {
            var imageUrl = "";
            try
            {
                var uri = new Uri(request.ImageUrl);
                imageUrl = uri.ToString();
            } catch(Exception ex)
            {
                imageUrl = null;
            }
            return new Recipe(request.Title, request.Description,request.Instructions, imageUrl, request.Ingredients, request.IsShared, userId);
        }
    }
}