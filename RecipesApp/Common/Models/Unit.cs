using System.Diagnostics.CodeAnalysis;

namespace RecipesApp.Common.Models
{
    /// <summary>
    /// Model defining the structure of units of measurements
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Unit
    {
        /// <summary>
        /// The name or abbreviation
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The base unit it relates to
        /// </summary>
        public BaseUnit BaseUnit { get; set; }

        /// <summary>
        /// The conversion factor between the unit and its base unit
        /// </summary>
        public decimal ConversionFactor { get; set; }
    }

    /// <summary>
    /// The available base units all other units are derived from
    /// </summary>
    public enum BaseUnit
    {
        /// <summary>
        /// SI unit for volume
        /// </summary>
        Liter,

        /// <summary>
        /// SI unit for mass
        /// </summary>
        Gram,

        /// <summary>
        /// base unit for common quantity types
        /// </summary>
        Tablespoon,

        /// <summary>
        /// Unit for amounts of ingredients
        /// </summary>
        Piece,

        /// <summary>
        /// Unit used for very small amounts
        /// </summary>
        Pinch,
    }
}