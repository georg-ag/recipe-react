using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RecipesApp.Recipes.Models;
using RecipesApp.Recipes.Services;

namespace RecipesApp.Controllers
{
    /// <summary>
    /// Controller for accessing and modifying recipes
    /// </summary>
    [Route("api/[controller]")]
    public class RecipesController : Controller
    {
        private readonly IRecipeService _service;

        /// <summary>
        /// Instantiates a <see cref="RecipesController"/>
        /// </summary>
        /// <param name="service">The <see cref="IRecipeService"/> required to work with recipes</param>
        public RecipesController(IRecipeService service)
        {
            this._service = service;
        }

        /// <summary>
        /// Gets a specific recipe by its <see cref="Guid"/>
        /// </summary>
        /// <param name="id">The unique <see cref="Guid"/> identifying the recipe</param>
        /// <returns>The <see cref="Recipe"/> object</returns>
        [HttpGet("{id:guid}", Name = nameof(GetRecipeById))]
        [ProducesResponseType(typeof(Recipe), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRecipeById(Guid id)
        {
            return this.Ok(await this._service.GetRecipeByIdAsync(id));
        }

        /// <summary>
        /// Gets all recipes a user has created
        /// </summary>
        /// <returns>An array of <see cref="Recipe"/></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(Recipe[]), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRecipes()
        {
            return this.Ok(await this._service.GetUserRecipesAsync());
        }

        /// <summary>
        /// Add a Recipe to a users collection
        /// </summary>
        /// <param name="request">The <see cref="CreateRecipe"/> request for the new recipe</param>
        /// <returns>The created <see cref="Recipe"/> and its location</returns>
        [HttpPost("")]
        [ProducesResponseType(typeof(Recipe), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(BadRequestObjectResult), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AddRecipe([FromBody] CreateRecipe request)
        {
            if (request != null)
            {
                var newRecipe = await this._service.AddRecipeAsync(request);
                return this.CreatedAtAction(nameof(this.GetRecipeById), new { id = newRecipe.Id }, newRecipe);
            }
            else
            {
                return this.BadRequest("invalid recipe received.");
            }
        }

        /// <summary>
        /// Updates a already existing <see cref="Recipe"/>
        /// </summary>
        /// <param name="id">The <see cref="Guid"/> identifying the recipe</param>
        /// <param name="recipe">The updated <see cref="Recipe"/> object</param>
        /// <returns>The updated <see cref="Recipe"/> object</returns>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(typeof(Recipe), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateRecipe(Guid id, [FromBody] Recipe recipe)
        {
            return this.Ok(await this._service.UpdateRecipeAsync(id, recipe));
        }

        /// <summary>
        /// Deletes a <see cref="Recipe"/>
        /// </summary>
        /// <param name="id">The <see cref="Guid"/> identifying the recipe</param>
        /// <returns>An <see cref="OkObjectResult"/></returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(typeof(OkResult), StatusCodes.Status204NoContent)]
        public async Task<IActionResult> DeleteRecipe(Guid id)
        {
            await this._service.DeleteRecipeAsync(id);
            return this.NoContent();
        }
    }
}